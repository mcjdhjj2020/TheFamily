define(function(require) {
	var ps = [];
	var _month = 1;//当前显示月
	var birthdayWall = {
		init : function(callback) {
			$('#death').hide();
			$('#keywords')[0].focus();
			birthdayWall.eventBind();
			birthdayWall.searchPerson(1);
			var date = new Date();
			//得到今天的农历月份
			$.ajax({
				type : 'POST',
				url : '/calendar/byYang.do',
				data : "&date=" + date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate(),
				dataType : 'json',
				success : function(req) {
					$('#todayStr').html('今天是 ' + req.yangStr + req.weekday + ' 农历' + req.nongStr);
					birthdayWall.monthSwitch(new Date(req.nong).getMonth() + 1);
				}
			});
		},
		eventBind : function() {
			$('#btn_search').click(function() {
				birthdayWall.searchPerson(1);
			});
		},
		searchPerson : function(page) {
			$.ajax({
				type : 'POST',
				url : '/person/searchList.do',
				data : '&keywords=' + $('#keywords').val() + "&page=" + page + '&rows=10',
				dataType : 'json',
				success : function(req) {
					ps = req.data;
					var dataHtml_left = '';
					var dataHtml_right = '';
					for (var i = 0; i < req.data.length; i++) {
						var p = req.data[i];
						var sex = p.sex == 'M' ? '男' : '女';
						var age = new Date().getFullYear() - new Date(p.birthday).getFullYear();
						if (i%2 == 0) {
							dataHtml_left += '<a href="javascript:birthdayWall.showPersonCard('+p.id+');" class="list-group-item"><strong>'+p.name+'</strong> '+sex+' '+age+'岁</a>';
						} else {
							dataHtml_right += '<a href="javascript:birthdayWall.showPersonCard('+p.id+');" class="list-group-item"><strong>'+p.name+'</strong> '+sex+' '+age+'岁</a>';
						}
					}
					$('#list_person_left').html(dataHtml_left);
					$('#list_person_right').html(dataHtml_right);
					birthdayWall.showPersonCard(0);
				}
			});
		},
		showPersonCard : function(id) {
			for (var i in ps) {
				if (i == id) {
					var p = ps[i];
					$('#name').html(p.name);
					if (p.deathday == null || p.deathday == '') {
						$('#death').hide();
						if (p.homeTell != '') {
							$('#homeTell').parent('p').show();
							$('#homeTell').html('<a href="tel:' + p.homeTell + '">' + p.homeTell + '</a>');
						} else {
							$('#homeTell').parent('p').hide();
						}
						if (p.phoneNumber != '') {
							$('#phoneNumber').parent('p').show();
							$('#phoneNumber').html('<a href="tel:' + p.phoneNumber + '">' + p.phoneNumber + '</a>');
						} else {
							$('#phoneNumber').parent('p').hide();
						}
					} else {
						$('#death').show();
						$('#homeTell').parent('p').hide();
						$('#phoneNumber').parent('p').hide();
					}
					$('#sex').html(p.sex == 'M' ? '男' : '女');
					var b = new Date(p.birthday);
					var age = new Date().getFullYear() - b.getFullYear();
					$('#age').html(age + '岁');
					$('#spouse').html(p.spouseId);
					var bYangStr = b.getFullYear() + '年' + (b.getMonth() + 1) + '月' + b.getDate() + '日';
					$('#yang').html(bYangStr);
					$('#nong').html(p.bNongStr);
					$.ajax({
						type : 'POST',
						url : '/calendar/next.do',
						data : '&nong=' + p.bNong.substr(p.bNong.indexOf('-')),
						dataType : 'json',
						success : function(req) {
							if (req == "0")
								$('#days').html('<font size="4">今日寿星</font>');
							else
								$('#days').html('<a>距下一次农历生日还有 <font size="4">'+req+'</font> 天</a>');
						}
					});
					break;
				}
			}
		},
		num2ch : function(moth) {
			switch (moth) {
			case 1: return '正月';
			case 2: return '二月';
			case 3: return '三月';
			case 4: return '四月';
			case 5: return '五月';
			case 6: return '六月';
			case 7: return '七月';
			case 8: return '八月';
			case 9: return '九月';
			case 10: return '十月';
			case 11: return '十一月';
			case 12: return '腊月';
			default: return 'X月';
			}
		},
		monthSwitch : function(month) {
			if (month == 0) {//上一月
				if (_month != 1)
					_month -= 1;
			} else if (month == 13) {//下一月
				if (_month != 12)
					_month += 1;
			} else				
				_month = month;
			var pHtml = '';
//			if (_month == 1)
//				pHtml += '<li class="disabled"><a href="javascript:wall(0);">&laquo;</a></li>';
//			else
//				pHtml += '<li><a href="javascript:wall(0);">&laquo;</a></li>';
			for (var i = 1; i <= 12; i++) {
				if (_month == i)
					pHtml += '<li class="active"><a href="javascript:wall('+i+');">'+birthdayWall.num2ch(i)+'</a></li>';
				else
					pHtml += '<li><a href="javascript:wall('+i+');">'+birthdayWall.num2ch(i)+'</a></li>';
			}
//			if (_month == 12)
//				pHtml += '<li class="disabled"><a href="javascript:wall(13);">&raquo;</a></li>';
//			else
//				pHtml += '<li><a href="javascript:wall(13);">&raquo;</a></li>';
			$('#list_birthday_page_up').html(pHtml);
			$('#list_birthday_page_down').html(pHtml);
			birthdayWall.loadBirthdayWall();
		},
		loadBirthdayWall : function() {
			$.ajax({
				type : 'POST',
				url : '/person/birthdayList.do',
				data : '&month=' + _month,
				dataType : 'json',
				success : function(req) {
					$('#list_birthday').html('');
					for(var i in req.data) {
						var p = req.data[i];
						var sex = p.sex == 'M' ? '男' : '女';
						var b = new Date(p.birthday);
						var age = new Date().getFullYear() - new Date(p.birthday).getFullYear();
						var dataHtml = '<p class="list-group-item"> <font size="3">'+p.name+'</font> '+sex+' '+age+'岁 '+p.bNongStr+' <span id="days_'+p.id+'"><a>距下一次农历生日还有...</a></span></p>';
						$('#list_birthday').append(dataHtml);
					}
					setTimeout(function() {
						for(var i in req.data) {
							var p = req.data[i];
							var nong = p.nong + "";
							$.ajax({
								type : 'POST',
								url : '/calendar/next.do',
								data : '&nong=' + p.bNong.substr(p.bNong.indexOf('-')),
								dataType : 'json',
								async: false,
								success : function(req) {
									var days = '';
									if (req == "0")
										days = '<a><font size="4">今日寿星</font></a>';
									else
										days = '<a>距下一次农历生日还有 <font size="4">'+req+'</font> 天</a>';
//									console.log(i + ' ' + p.id + ' ' + days);
									$('#days_' + p.id).html(days);
								}
							});
						}
					}, 10);
				}
			});
		}
	}
	return birthdayWall;
});
function wall(month) {
	birthdayWall.monthSwitch(month);
}