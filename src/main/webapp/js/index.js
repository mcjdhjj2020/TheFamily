// 预加载
requirejs.config({
	baseUrl : 'js',
	paths : {
		"jquery" : "lib/jquery-1.8.3",
		"jquery.ui" : "lib/jquery-ui",
		"jquery.form" : "lib/jquery.form",
		"jquery.cookie" : "lib/jquery.cookie"
	},
	shim : {
		'jquery.ui' : [ 'jquery' ],
		'jquery.form' : [ 'jquery' ],
		'jquery.cookie' : [ 'jquery' ]
	}
});
// 预初始化
var myFamily = null;
var familyTree = null;
var birthdayWall = null;
function loadJsMode() {
	require([ "jquery", "myFamily", "familyTree", "birthdayWall", "jquery.cookie", "jquery.ui" ], function($, myFamilyMode, familyTreeMode, birthdayWallMode) {
		myFamily = myFamilyMode;
		familyTree = familyTreeMode;
		birthdayWall = birthdayWallMode;
	});
}

// 本页初始化
var indexPage = {
	init : function() {
		var _this = indexPage;
		if (true) {// 验证状态
			_this.menuSelect("welcome");
			$('#main-content').load('welcome.html');
			_this.eventBind();
			loadJsMode();
		} else {
			location.href = "/";
		}
	},
	menuSelect : function(name) {
		$('#welcome').parent("li").attr("class", "");
		$('#myFamily').parent("li").attr("class", "");
		$('#familyTree').parent("li").attr("class", "");
		$('#birthdayWall').parent("li").attr("class", "");
		$('#' + name).parent("li").attr("class", "nav-current");
	},
	eventBind : function() {
		var _this = indexPage;
		$('#welcome').click(function() {
			_this.menuSelect("welcome");
			$('#main-content').load('welcome.html');
		});
		$('#myFamily').click(function() {
			_this.menuSelect("myFamily");
			$('#main-content').load('myFamily.html', function() {
				myFamily.init();
			});
		});
		$('#familyTree').click(function() {
			_this.menuSelect("familyTree");
			$('#main-content').load('familyTree.html', function() {
				familyTree.init();
			});
		});
		$('#birthdayWall').click(function() {
			_this.menuSelect("birthdayWall");
			$('#main-content').load('birthdayWall.html', function() {
				birthdayWall.init();
			});
		});
	}
}

// 开始
$(function() {
	indexPage.init();
});
