package com.liuzy.family.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.servlet.ServletContextEvent;

import org.apache.log4j.PropertyConfigurator;
import org.springframework.web.context.ContextLoaderListener;

public class BeforeContextLoaderListener extends ContextLoaderListener {

	private static String appCfg;

	private static String LOG4J = "log4j.properties";

	@Override
	public void contextInitialized(ServletContextEvent event) {
		// 配置文件路径
		String classpath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
		if (!classpath.contains("webapps")) {
			appCfg = classpath.substring(1, classpath.indexOf("target")) + "appCfg" + File.separator;
		} else {
			String temp = classpath.substring(0, classpath.indexOf("webapps"));
			appCfg = temp.substring(0, temp.lastIndexOf("/")) + File.separator + "appCfg" + File.separator;
		}
		System.setProperty("appCfg", appCfg);

		// log4j配置
		try {
			FileInputStream inputStream = new FileInputStream(appCfg + LOG4J);
			PropertyConfigurator.configure(inputStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		super.contextInitialized(event);
	}

	public static String getAppCfg() {
		return appCfg;
	}

}
