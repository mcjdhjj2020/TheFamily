package com.liuzy.family.vo;

import com.liuzy.family.entity.PersonEntity;

/**
 * 搜索时传入对象
 */
public class PersonVo extends PersonEntity {

	private String babaName;

	private String mamaName;

	private String spouseName;

	public String getBabaName() {
		return babaName;
	}

	public void setBabaName(String babaName) {
		this.babaName = babaName;
	}

	public String getMamaName() {
		return mamaName;
	}

	public void setMamaName(String mamaName) {
		this.mamaName = mamaName;
	}

	public String getSpouseName() {
		return spouseName;
	}

	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}
}
