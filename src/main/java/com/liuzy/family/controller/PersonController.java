package com.liuzy.family.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.liuzy.family.entity.PersonEntity;
import com.liuzy.family.mapper.PersonEntityMapper;
import com.liuzy.family.service.CalendarService;
import com.liuzy.family.util.StrKit;
import com.liuzy.family.vo.BaseVo;
import com.liuzy.family.vo.PersonVo;

@Controller
@RequestMapping(value = "/person", method = RequestMethod.POST)
public class PersonController extends BaseController {

	@Autowired
	private CalendarService calendarService;
	
	@Autowired
	private PersonEntityMapper personEntityMapper;

	@RequestMapping("/saveOrUpdate")
	@ResponseBody
	public Object add(@RequestBody(required = false) PersonEntity e) {
		if (e == null) {
			return render("error");
		}
		if (e.getId() == null) {
			return render(personEntityMapper.insert(e));
		} else {
			return render(personEntityMapper.updateByPrimaryKey(e));
		}
	}

	@RequestMapping("/del")
	@ResponseBody
	public Object del(Integer id) {
		if (id == null) {
			return render("error");
		}
		return render(personEntityMapper.deleteByPrimaryKey(id));
	}
	
	@RequestMapping("/searchList")
	@ResponseBody
	public Object searchList(
			String keywords, 
			String sex, 
			@RequestParam(defaultValue="1") int page, 
			@RequestParam(defaultValue="10") int rows) {
		if (keywords == null) {
			return render("error");
		}
		Map<String, Object> params = new HashMap<>();
		params.put("keywords", keywords);
		params.put("sex", sex);
		Integer total = personEntityMapper.searchListCount(params);
		List<PersonEntity> es = personEntityMapper.searchList(params, new RowBounds((page - 1) * rows, rows));
		List<PersonVo> ps = new ArrayList<>();
		for (PersonEntity e : es) {
			PersonVo p = new PersonVo();
			p.setId(e.getId());
			p.setName(e.getName());
			p.setFormerName(e.getFormerName());
			p.setNickName(e.getNickName());
			p.setSex(e.getSex());
			p.setBirthday(e.getBirthday());
			p.setbNong(e.getbNong());
			p.setbNongStr(e.getbNongStr());
			p.setDeathday(e.getDeathday());
			p.setdNong(e.getdNong());
			p.setdNongStr(e.getdNongStr());
			p.setHomeAddress(e.getHomeAddress());
			p.setHomeTell(e.getHomeTell());
			p.setWorkUnits(e.getWorkUnits());
			p.setWorkAddress(e.getWorkAddress());
			p.setPhoneNumber(e.getPhoneNumber());
			String babaId = e.getBabaId();
			if (!StrKit.isBlank(babaId)) {
				p.setBabaId(babaId );
				PersonEntity baba = personEntityMapper.selectByPrimaryKey(StrKit.firstId(babaId));
				if (baba != null) {
					p.setBabaName(baba.getName());
				}
			}
			String mamaId = e.getMamaId();
			if (!StrKit.isBlank(mamaId)) {
				p.setMamaId(mamaId);
				PersonEntity mama = personEntityMapper.selectByPrimaryKey(StrKit.firstId(mamaId));
				if (mama != null) {
					p.setMamaName(mama.getName());
				}
			}
			String spouseId = e.getSpouseId();
			if (!StrKit.isBlank(spouseId)) {
				p.setSpouseId(spouseId);
				PersonEntity se = personEntityMapper.selectByPrimaryKey(StrKit.firstId(spouseId));
				if (se != null) {
					p.setSpouseName(se.getName());
				}
			}
			p.setTbId(e.getTbId());
			p.setZnId(e.getZnId());
			ps.add(p);
		}
		BaseVo vo = new BaseVo(page, rows);
        vo.setData(ps);
        vo.setCount(ps.size());
        vo.setTotalCount(total);
        return render(vo);
	}
	
	@RequestMapping("/birthdayList")
	@ResponseBody
	public Object birthdayList(String month) {
		List<PersonEntity> ps = personEntityMapper.listBirthdayByMonth(month);
		BaseVo vo = new BaseVo();
        vo.setData(ps);
        vo.setCount(ps.size());
        vo.setTotalCount(ps.size());
		return render(vo);
	}
}
