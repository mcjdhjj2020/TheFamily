package com.liuzy.family.util;

public class StrKit {
	public static boolean isBlank(String str) {
		return str == null || "".equals(str.trim()) ? true : false;
	}
	public static boolean notBlank(String str) {
		return str == null || "".equals(str.trim()) ? false : true;
	}
	public static Integer firstId(String ids) {
		return Integer.parseInt(ids.split(",")[0]);
	}
}
