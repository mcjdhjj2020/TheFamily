package com.liuzy.family.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.liuzy.family.entity.CalendarEntity;
import com.liuzy.family.mapper.CalendarEntityMapper;
import com.liuzy.family.util.DateKit;

@Service
public class CalendarService {

	@Autowired
	private CalendarEntityMapper calendarEntityMapper;

	// 返回:1901年1月4日 星期五_庚子年(1900)十一月十四
	public String getBirthday(Date date) {
		String dateStr = DateKit.format(date);
		String birthday = "";
		if (dateStr != null) {
			dateStr = dateStr.replace("-0", "-");
			CalendarEntity ce = calendarEntityMapper.getByYang(dateStr);
			if (ce != null) {
				birthday = ce.getYangStr() + " " + ce.getWeekday() + "_" + ce.getNongStr();
			}
		}
		return birthday;
	}
	
}
