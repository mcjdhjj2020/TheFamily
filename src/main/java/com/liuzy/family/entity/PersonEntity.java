package com.liuzy.family.entity;

import java.util.Date;

public class PersonEntity {
    private Integer id;

    private String name;

    private String formerName;

    private String nickName;

    private String sex;

    private Date birthday;

    private String bNong;

    private String bNongStr;

    private Date deathday;

    private String dNong;

    private String dNongStr;

    private String homeAddress;

    private String homeTell;

    private String workUnits;

    private String workAddress;

    private String phoneNumber;

    private String babaId;

    private String mamaId;

    private String spouseId;

    private String tbId;

    private String znId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormerName() {
        return formerName;
    }

    public void setFormerName(String formerName) {
        this.formerName = formerName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getbNong() {
        return bNong;
    }

    public void setbNong(String bNong) {
        this.bNong = bNong;
    }

    public String getbNongStr() {
        return bNongStr;
    }

    public void setbNongStr(String bNongStr) {
        this.bNongStr = bNongStr;
    }

    public Date getDeathday() {
        return deathday;
    }

    public void setDeathday(Date deathday) {
        this.deathday = deathday;
    }

    public String getdNong() {
        return dNong;
    }

    public void setdNong(String dNong) {
        this.dNong = dNong;
    }

    public String getdNongStr() {
        return dNongStr;
    }

    public void setdNongStr(String dNongStr) {
        this.dNongStr = dNongStr;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getHomeTell() {
        return homeTell;
    }

    public void setHomeTell(String homeTell) {
        this.homeTell = homeTell;
    }

    public String getWorkUnits() {
        return workUnits;
    }

    public void setWorkUnits(String workUnits) {
        this.workUnits = workUnits;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBabaId() {
        return babaId;
    }

    public void setBabaId(String babaId) {
        this.babaId = babaId;
    }

    public String getMamaId() {
        return mamaId;
    }

    public void setMamaId(String mamaId) {
        this.mamaId = mamaId;
    }

    public String getSpouseId() {
        return spouseId;
    }

    public void setSpouseId(String spouseId) {
        this.spouseId = spouseId;
    }

    public String getTbId() {
        return tbId;
    }

    public void setTbId(String tbId) {
        this.tbId = tbId;
    }

    public String getZnId() {
        return znId;
    }

    public void setZnId(String znId) {
        this.znId = znId;
    }
}